\babel@toc {portuguese}{}
\beamer@sectionintoc {1}{Introdução}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Imagens Ponderadas em Difusão}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Movimento Incoerente dos Spins}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Aplicações de Deep Learning}{5}{0}{1}
\beamer@sectionintoc {2}{Materiais e Métodos}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{Métodos de Ajuste de Curva}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Deep Learning}{8}{0}{2}
\beamer@sectionintoc {3}{Resultados e Discussões}{9}{0}{3}
\beamer@sectionintoc {4}{Conclusões}{10}{0}{4}
\beamer@sectionintoc {5}{Agradecimentos}{11}{0}{5}
